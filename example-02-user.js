// Shows how you can export an object prototype

var User = function (id, name) {
    this.id = id
    this.name = name;
    this.userInfo = function () {
        return this.id + ' ' + this.name;
    };
};
module.exports = User;
