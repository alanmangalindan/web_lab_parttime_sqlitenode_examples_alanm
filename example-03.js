
// Setup code
// ------------------------------------------------------------------------------
var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

// Use our custom DAO module
var articleDAO = require('./example-03-dao-module.js');
// ------------------------------------------------------------------------------


// Route handlers
// ------------------------------------------------------------------------------

// When the user navigates to "/", load some articles and display their summary to the user.
app.get(['/', '/articles'], function (req, res) {

    articleDAO.getAllArticles(function (articles) {
        res.render("articles", { articles: articles });
    });

});

// When the user navigates to "/articles/:id", load the article with the given id and display it to the user.
app.get('/articles/:id', function (req, res) {

    articleDAO.getArticle(req.params.id, function (article) {
        res.render("article-detail", { title: article.title, content: article.content });
    });

});

// When the user POSTs to "/articles", add a new article, then redirect back to articles display.
app.post('/articles', function (req, res) {

    var articleTitle = req.body.title;
    var articleContent = req.body.content;

    articleDAO.addArticle(articleTitle, articleContent, function (id) {

        console.log("New article added with id = " + id);

        res.redirect("/articles");
    });
});

// When the user POSTs to "/delete/:id", delete the article with the given id, then redirect to /articles.
app.post('/delete/:id', function (req, res) {

    var articleId = req.params.id;

    articleDAO.deleteArticle(req.params.id, function (rowsAffected) {

        console.log(rowsAffected + " row(s) affected.");

        res.redirect('/articles');
    });
});

// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));
// ------------------------------------------------------------------------------


// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});