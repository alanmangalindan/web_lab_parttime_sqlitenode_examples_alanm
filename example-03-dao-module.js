// Allow us to use SQLite3 from node.js
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('example-01.db');

// A function which loads all articles from the database, then sends them to the given callback.
module.exports.getAllArticles = function (callback) {
    db.all("SELECT id, title, substr(content, 1, 100) || '...' as 'content' FROM articles", function (err, rows) {

        // console.log(rows);

        // "rows" is a JavaScript array, and we can do anything we would normally do for such an array.
        callback(rows);
    });
};

// A function which gets the article with the given id from the database, then sends it to the given callback.
module.exports.getArticle = function (id, callback) {
    db.all("SELECT * FROM articles WHERE id = ?", [id], function (err, rows) {

        var article = rows[0]; // There should only be one...

        callback(article);
    });
};

// A function that adds a new article with the given title and content to the database, then sends its automatically generated id to the given callback.
module.exports.addArticle = function (title, content, callback) {
    db.run("INSERT INTO articles (title, content) VALUES (?, ?)", [title, content], function (err) {

        // Within this function, you can use this.lastID to get the automatically-generated ID that was just inserted into the db.
        // console.log("New article added with id = " + this.lastID);

        // Within this function, you can use this.changes to get the number of rows affected by the query.
        // console.log(this.changes + " row(s) affected.");

        callback(this.lastID);
    });
};

// A function that deletes the article with the given id from the database, then sends the number of rows affected (probably 0 or 1) to the given callback.
module.exports.deleteArticle = function (id, callback) {
    db.run("DELETE FROM articles WHERE id = ?", [id], function (err) {

        // Within this function, you can use this.changes to get the number of rows affected by the query.
        // console.log(this.changes + " row(s) affected.");

        callback(this.changes);
    });
};